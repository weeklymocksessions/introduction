function reducer(state, action) {

    console.log(action);
    console.log(state);

    switch (action.type) {
        case 'INC':
            return { ...state,  counter: state.counter + 1 };
        case 'DEC':
            return { ...state, counter: state.counter - 1 };
        default:{
            console.log('default action');
            return state;
        }
    }
}