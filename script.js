// Update view (this might be React in a real app)
function updateView() {
    document.querySelector('#counter').innerText = store.getState().counter;
}

store.subscribe(updateView);

// Update view for the first time
 updateView();

// Listen to click events
document.getElementById('inc').onclick = () => store.dispatch({ type: 'INC' });
document.getElementById('dec').onclick = () => store.dispatch({ type: 'DEC' });
document.getElementById('und').onclick = () => store.dispatch({ type: 'UND' });